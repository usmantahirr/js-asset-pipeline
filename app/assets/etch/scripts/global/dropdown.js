(function() {
  "use strict";

  var Etch = window.Etch || {};

  var dropdown = {
    container: document,
    $document: $(document),

    init: function(container) {
      _.bindAll(this, 'closeOpenMenus');
      this.container = container;
      this.$document.on('click', this.closeOpenMenus);
      this.wireDropToggle();
    },

    update: function(options) {
      this.$document.off('.etch-dropdown');
      if (options.container) {
        this.container = options.container;
      }
      this.wireDropToggle();
    },

    wireDropToggle: function() {
      var self = this;

      this.$document.off('.etch-dropdown')
        .on('click.etch-dropdown', '[data-etch-dropdown]', function(e) {
          var $toggle = $(this),
            $dropdown = self.getDropdown($toggle),
            dropdownCallback = $toggle.data('etch-dropdown-callback');

          e.preventDefault();
          self.closeOpenMenus(e);
          self.toggleDropClass($dropdown, $toggle, dropdownCallback);
        });
    },

    getDropdown: function($toggle) {
      var $parentSelector = $toggle.data('etch-dropdown-parent'),
        $parent = $parentSelector ? $toggle.parents($parentSelector).first() : $toggle,
        $dropdownSelector = $toggle.data('etch-dropdown');

      return $parent.find($dropdownSelector).first();
    },

    toggleDropClass: function($dropdown, $toggle, cb) {
      var transEnd = ['webkitTransitionEnd', 'otransitionend', 'oTransitionEnd', 'msTransitionEnd', 'transitionend'],
        listenerNamespace = '.etch-transition-listener',
        self = this,
        isOpening = !$dropdown.hasClass('etch-dropdown--open'),
        data = {
          dropdown: $dropdown[0],
          toggle: $toggle[0]
        };

      $toggle.trigger('dropdownTransitionStart.etch-dropdown-event', [data]);
      $dropdown.trigger('dropdownTransitionStart.etch-dropdown-event', [data]);
      $toggle.toggleClass('etch-toggler--open');
      $toggle.addClass('etch-dropdown--transitioning');
      $dropdown.addClass('etch-dropdown--transitioning');
      $dropdown.toggleClass('etch-dropdown--open');

      if (isOpening) {
        $toggle.trigger('openDropdown.etch-dropdown-event', [data]);
        $dropdown.trigger('openDropdown.etch-dropdown-event', [data]);
        this.checkOppositeOpen($dropdown);
        if (cb) {
          self.executeFunctionByName(cb, data);
        }
      } else {
        $toggle.trigger('closeDropdown.etch-dropdown-event', [data]);
        $dropdown.trigger('closeDropdown.etch-dropdown-event', [data]);
        $dropdown.removeClass('etch-dropdown--open-x etch-dropdown--open-y etch-dropdown--opened');
        $toggle.removeClass('etch-toggler--opened');
      }

      transEnd = _.map(transEnd, function(transition) {
        return transition + listenerNamespace;
      }).join(' ');
      $dropdown.off(listenerNamespace);
      $dropdown.on(transEnd, function(e) {
        $dropdown.off(listenerNamespace);
        $dropdown.removeClass('etch-dropdown--transitioning');
        $toggle.removeClass('etch-dropdown--transitioning');
        if (isOpening) {
          $dropdown.addClass('etch-dropdown--opened');
          $toggle.addClass('etch-toggler--opened');
          $toggle.trigger('dropdownOpened.etch-dropdown-event', [data]);
          $dropdown.trigger('dropdownOpened.etch-dropdown-event', [data]);
        } else {
          $dropdown.removeClass('etch-dropdown--opposite-x');
          $toggle.trigger('dropdownClosed.etch-dropdown-event', [data]);
          $dropdown.trigger('dropdownClosed.etch-dropdown-event', [data]);
        }
        $toggle.trigger('dropdownTransitionEnd.etch-dropdown-event', [data]);
        $dropdown.trigger('dropdownTransitionEnd.etch-dropdown-event', [data]);
      });
    },

    checkOppositeOpen: function($dropdown) {
      var container = $dropdown.data('etch-dropdown-container'),
        $container = container ? $(container) : $(this.container),
        elHeight = $dropdown.prop('scrollHeight'),
        elWidth = $dropdown.prop('scrollWidth'),
        topDistance = $dropdown.offset().top - this.$document.scrollTop(),
        bottomDistance = $container.height() - topDistance,
        leftDistance = $dropdown.offset().left - this.$document.scrollLeft(),
        rightDistance = $container.width() - leftDistance;

      if (elWidth >= rightDistance || $dropdown.hasClass('pull-right')) {
        $dropdown.addClass('etch-dropdown--open-x etch-dropdown--opposite-x');
      }
    },

    closeOpenMenus: function(e) {
      var self = this,
        $openToggles = $('.etch-toggler--open'),
        $dropdown, $toggle, persist, dropdownClicked;

      $openToggles.each(function(i, toggle) {
        $toggle = $(toggle);
        $dropdown = self.getDropdown($toggle);
        dropdownClicked = $dropdown.is(e.target) || $dropdown.find(e.target).length;
        persist = $dropdown.data('etch-dropdown-persist') && dropdownClicked;

        // automatically close menus or toggles that have been clicked in, unless the persist option or the sticky option is in place
        if (!$toggle.is(e.target) && !$toggle.find(e.target).length && !$dropdown.attr('data-etch-dropdown-sticky') && !persist) {
          self.toggleDropClass($dropdown, $toggle);
        }
      });
    },

    executeFunctionByName: function(functionName, data) {
      var context = window,
        namespaces = functionName.split('.'),
        func = namespaces.pop(),
        i = 0;

      while (i < namespaces.length) {
        context = context[namespaces[i]];
        i++;
      }

      return context[func].apply(this, [data]);
    }
  };

  window.Etch = _.assign(Etch, {dropdown: dropdown});

})();
