(function(){
  "use strict";

  var Etch = window.Etch || {};
  Etch.imageRoot = Etch.imageRoot || '/images/';


  var donutChart = {

    width: 180,
    height: 180,
    vpad: 40,
    hpad: 80,
    depth: 12,
    data: [],
    duration: 1500, // transition duration in milliseconds
    chartText: '',
    chart: null,
    oldPieData: [],

    // ---  Computed Properties  ---
    pie: d3.layout.pie().sort(null).value(function(d) {
      return d.count;
    }),

    radius: function() {
      return Math.min(80, Math.min(this.width, this.height));
    },

    translation: function() {
      var radius = this.radius();

      return 'translate(' + Number(radius + this.hpad / 2) +
        ',' + Number(radius + this.vpad / 2) + ')';
    },

    arc: function() {
      var radius = this.radius();
      return d3.svg.arc().innerRadius(radius - this.depth).outerRadius(radius);
    },

    calculatedTotal: function() {
      if (this.data) {
        return _.reduce(this.data, function(memo, entry) {
          return memo + parseInt(entry.count, 10);
        }, 0);
      }
      return 0;
    },


    // ---  Methods  ---
    init: function(el, opts) {
      if(!el || !opts.data) return false;
      this.$el = $(el);
      this.data = opts.data;
      this.chartText = opts.chartText;

      _.bindAll(this);
      this.checkForFilters();
      this.render();
    },

    render: function() {
      this.renderChart();
      return this;
    },

    smoothUpdate: function() {
      var svg = this.chart;

      if (!svg || svg.classed('etch-donut-chart--no-data')) {
        return this.renderChart();
      }

      this.createPie(svg);
      this.updateAlertCount();

      return this;
    },

    checkForFilters: function() {
      if ($('filter#subtleDropShadow').length < 1) {
        $.get( Etch.imageRoot + "filterSubtleDropShadow.svg", function( data ) {
          $('body').append( data );
        }, "html");
      }

      if ($('filter#pie-shrink').length < 1) {
        $.get( Etch.imageRoot + "filterPieShrink.svg", function( data ) {
          $('body').append( data );
        }, "html");
      }
    },

    empty: function() {
      if (this.chart) {
        this.chart.remove();
        this.chart = null;
        this.oldPieData = [];
        this.$el.contents().off();
        this.$el.empty();
      }
    },

    renderChart: function() {
      var svg = this.chart,
        translation = this.translation(),
        circle;

      this.empty();

      if (!this.$el.length) {
        return;
      }

      // Build Graph
      svg = d3.select(this.$el[0]).append('svg')
        .attr('width', this.width + this.hpad)
        .attr('height', this.height + this.vpad);

      circle = svg.append('g')
        .attr('transform', translation);

      circle.append('circle')
        .attr('class', 'etch-donut-chart__background')
        .attr('cx', 0)
        .attr('cy', 0)
        .attr('r', this.radius() - (this.depth / 2))
        .attr('fill-opacity', 0)
        .transition()
        .duration(500)
        .attrTween('fill-opacity', this.fadeTween);

      this.chart = svg;

      svg.append('text')
        .attr('dx', 0)
        .attr('dy', 5)
        .attr('class', 'etch-donut-chart__totalcount')
        .attr('transform', translation)
        .transition()
        .duration(1500)
        .attrTween('fill-opacity', this.fadeTween);

      svg.append('text')
        .attr('dx', 0)
        .attr('dy', 20)
        .attr('class', 'etch-donut-chart__totallabel')
        .attr('transform', translation)
        .attr('fill-opacity', 0)
        .transition()
        .delay(500)
        .duration(1500)
        .attrTween('fill-opacity', this.fadeTween);

      this.createPie();

      // Set the alert count text in the center of the chart
      this.updateAlertCount();

    },

    createPie: function() {
      var svg = this.chart,
        paths = svg.selectAll('g.arc path'),
        total = this.calculatedTotal(),
        txt, processedData;

      // add index and percentages to the data
      processedData = this.processData();

      // add new pie slices, remove slices if necessary
      this.addPiePieces(processedData);

      // modify existing pie slices
      if (paths.length) {
        this.oldPieData = paths.data();
      }
      this.animatePiePieces(svg, processedData);
      txt = svg.selectAll('g.arc text')
        .data(this.pie(processedData));
      this.textUpdate(txt);

      if (total === 0) {
        svg.attr('class', 'etch-donut-chart--no-data');
      } else {
        svg.attr('class', 'etch-donut-chart');
      }
    },

    // add new pie pieces to the chart
    addPiePieces: function(data) {
      var svg = this.chart;
      var g = svg.selectAll('g.arc')
        .data(this.pie(data));
      var newG = g.enter()
        .append('g')
        .attr('class', 'arc')
        .attr('transform', this.translation());
      newG.append('g')
        .attr('filter', 'url(#subtleDropShadow)')
        .append('path')
        .attr('d', this.arc())
        .attr('id', function(d) {
          return 'arc-' + d.data.index;
        })
        .attr('filter', 'url(#pie-shrink)')
        .attr('class', function(d) {
          if (d.data.className) {
            return d.data.className;
          }
          return 'etch-donut-chart__alert--rank' + d.data.index;
        });
      newG.append('text')
        .attr('dy', '0.4ex')
        .attr('class', 'etch-donut-chart__label');

      g.exit().remove();
    },

    animatePiePieces: function(svg, data) {
      svg.selectAll('g.arc path')
        .data(this.pie(data))
        .transition()
        .duration(this.duration)
        .attrTween('d', this.pieTween);
    },

    // update and reposition the percentages of each source
    textUpdate: function(txt) {
      txt.style('text-anchor', function(d) {
        return (d.endAngle + d.startAngle) / 2 > Math.PI ? 'end' : 'start';
      })
        .text(function(d) {
          return (d.data.percent.toFixed(0));
        });
      txt.append('tspan')
        .attr('class', 'etch-donut-chart__label__units')
        .text('%');
      txt.transition()
        .duration(this.duration)
        .attrTween('transform', this.textTween)
        .attrTween('fill-opacity', this.fadeTween);
    },

    // Tweening functions for animated parameters
    pieTween: function(d, i) {
      var s0 = 0,
        e0 = 0,
        arc = this.arc(),
        oldPieData = this.oldPieData,
        fn;

      if (!arc) {
        return;
      }

      if (oldPieData[i]) {
        s0 = oldPieData[i].startAngle;
        e0 = oldPieData[i].endAngle;
      } else if (_.last(oldPieData)) {
        s0 = _.last(oldPieData).endAngle;
        e0 = _.last(oldPieData).endAngle;
      }

      fn = d3.interpolate({
        startAngle: s0,
        endAngle: e0
      }, {
        startAngle: d.startAngle,
        endAngle: d.endAngle
      });

      return function(t) {
        var b = fn(t);
        return arc(b);
      };
    },

    processData: function() {
      var total = this.calculatedTotal(),
        processedData = [];

      _.each(this.data, function(point, idx, iter) {
        if (point.count) {
          point.percent = (point.count / total) * 100;
          point.index = idx;
          processedData.push(point);
        }
      });

      return processedData;
    },

    textTween: function(d, i) {
      var oldPieData = this.oldPieData,
        a = 0 - (Math.PI / 2),
        b = (d.startAngle + d.endAngle - Math.PI) / 2;

      if (oldPieData[i]) {
        a = (oldPieData[i].startAngle + oldPieData[i].endAngle - Math.PI) / 2;
      } else if (_.last(oldPieData)) {
        a = d.endAngle - (Math.PI / 2);
      }
      var fn = d3.interpolateNumber(a, b),
        labelRadius = this.radius() + 5;
      return function(t) {
        var val = fn(t);
        return 'translate(' + Math.cos(val) * labelRadius * 0.95 + ',' + Math.sin(val) * labelRadius * 1.05 + ')';
      };
    },

    fadeTween: function(d, i) {
      var fn = d3.interpolateNumber(0, 0.995);
      return function(t) {
        return fn(t);
      };
    },

    updateAlertCount: function() {
      var svg = this.chart,
        total = this.calculatedTotal(),
        totalCount, totalLabel;

      // check to make sure that the chart and alertCount exist
      if (!svg || (!_.isNumber(total) && !_.isString(total))) {
        return;
      }

      totalCount = svg.selectAll('.etch-donut-chart__totalcount');
      totalLabel = svg.selectAll('.etch-donut-chart__totallabel');

      totalLabel.text(this.chartText);
      if (total > 999) {
        total = numeral(total).format('0.0a');
      }
      totalCount.text(total);
    }
  };



  window.Etch = _.assign(Etch, {donutChart: donutChart});

})();
