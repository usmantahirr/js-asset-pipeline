(function(){
  "use strict";

  var Etch = window.Etch || {};




  var sparklineChart = {

    width: 30,
    height: 14,
    data: [],
    chart: null,
    accessor: {
      x: function(datum) {
        return datum.index;
      },
      y: function(datum) {
        return datum.yValue;
      }
    },

    init: function(el, opts) {
      if(!el || !opts.data) return false;
      this.$el = $(el);
      this.data = opts.data;

      _.bindAll(this);
      this.renderChart();
      return this;
    },

    renderChart: function() {
      var svg = this.chart,
        interpolation = this.interpolation,
        xRange, yRange,
        line, area;

      this.empty();

      if(!this.$el.length) {
        return;
      }

      // Ensure the data has an index property for the x values
      _.each(this.data, function(element, index) {
        element.index = index;
      });

      xRange = d3.scale.linear().range([0,this.width]);
      yRange = d3.scale.linear().range([this.height,0]);

      line = d3.svg.line()
        .interpolate(interpolation)
        .x(_.compose(xRange, this.accessor.x))
        .y(_.compose(yRange, this.accessor.y));

      area = d3.svg.area()
        .interpolate(interpolation)
        .x(_.compose(xRange, this.accessor.x))
        .y0(this.height)
        .y1(_.compose(yRange, this.accessor.y));

      xRange.domain(d3.extent(this.data, this.accessor.x));
      yRange.domain(d3.extent(this.data, this.accessor.y));

      svg = d3.select(this.$el[0])
        .append('svg')
        .attr('width', this.width)
        .attr('height', this.height)
        .attr('class', 'etch-sparkline-chart');
      svg.classed('etch-sparkline-chart--no-data', _.isEmpty(this.data));

      this.chart = svg;

      svg.append('path')
        .datum(this.data)
        .attr('class', 'etch-sparkline-chart__line')
        .attr('d', line);

      svg.append('path')
        .datum(this.data)
        .attr('class', 'etch-sparkline-chart__area')
        .attr('d', area);
    },

    empty: function() {
      if (this.chart) {
        this.chart.remove();
        this.chart = null;
        this.$el.contents().off();
        this.$el.empty();
      }
    },

    destroy: function() {
      this.empty();
      this.$el.off();
      this.$el.empty();
      return this;
    }
  };






  window.Etch = _.assign(Etch, {sparklineChart: sparklineChart});

})();
