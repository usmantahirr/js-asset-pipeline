(function(){
  "use strict";

  var Etch = window.Etch || {};



  var areaChart = {
    data: [],
    chart: null,
    $el: null,

    init: function(el, opts) {
      if(!el || !opts.data) return false;

      this.$el = $(el);
      this.data = opts.data;

      _.bindAll(this);
      this.renderChart();
      return this;
    },

    renderChart: function() {
      var types = {};
      var columns = this.data.map(function(dataSet){
        dataSet.points.unshift(dataSet.name);
        types[dataSet.name] = 'area-spline';
        return dataSet.points;
      });
      var args = {
        data: {
          columns: columns,
          types: types
        },
        axis: {
          x: {
            min: 1,
            tick: {
              outer: false
            }
          }
        },
        grid: {
          y: {
            show: true
          }
        },
        area: {
          zerobased: true
        },
        point: {
          show: false
        }
      };
      this.chart = c3.generate(args);
      this.chart.element.className = this.chart.element.className + " etch-c3-area-chart";
      this.$el.html(this.chart.element);
    },

    destroy: function() {
      this.$el.remove();
      this.$el = null;
    }
  };



  window.Etch = _.assign(Etch, {areaChart: areaChart});

})();
