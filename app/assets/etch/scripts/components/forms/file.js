(function(){
  "use strict";

  var Etch = window.Etch || {};

  var fileUpload = {

    init: function(el) {

      $(document).on('click', '.etch-file--upload .etch-btn', function(e) {
        e.preventDefault();
        $(e.currentTarget).parent()
                          .find('input[type="file"]')
                          .trigger('click');
      });

      $(document).on('click', '.etch-file--field .etch-file--output', function(e) {
        e.preventDefault();
        $(e.currentTarget).parent()
                          .prev('.etch-file--upload')
                          .find('input[type="file"]')
                          .trigger('click');
      });

      $(document).on('change', '.etch-file--upload input[type=file]', function(e) {
        $(this).parent()
               .next('.etch-file--field')
               .find('input[type="text"]')
               .val( $(this).val().split('\\').pop() );
      });
    }
  };

  window.Etch = _.assign(Etch, {fileUpload: fileUpload});

})();
