(function(){
  "use strict";

  var Etch = window.Etch || {};

  var tabs = {
    $el: null,
    clickFire: false,
    updateHash: true,

    init: function(options) {
      this.$el = $('.etch-tabs');
      var self = this;
      _.bindAll(this);

      if(options) {
        if(options.hasOwnProperty('$el')) {
          this.$el = options.$el;
        }

        if(options.hasOwnProperty('updateHash')) {
          this.updateHash = options.updateHash;
        }
      }


      this.$el.on('click.etch-tabs', '.etch-tabs__anchor', function(e) {
        if(!self.updateHash) e.preventDefault();
        self.clickFire = true;
        self.wireTabClick(e, self);
      });

      if(this.updateHash) {
        $(window).on('hashchange.etch-tabs', function(e) {
          self.wireHashChange(self);
        });
      }

      this.activateFromHash();
    },

    wireTabClick: function(e, _this) {
      var activeTab = $(e.target).data('etch-tabs-anchor');
      _this.toggleVisible(activeTab);
    },

    wireHashChange: function(_this) {
      var activeTab = window.location.hash.replace('#', '');

      if(activeTab.search('-tab') > -1 && !_this.clickFire){
        _this.toggleVisible(activeTab);
      }

      _this.clickFire = false;
    },

    toggleVisible: function(activeTab) {
      var $tabContent = this.$el.find('[data-etch-tabs="' + activeTab + '"]');
      var $tabAnchor = this.$el.find('[data-etch-tabs-anchor="' + activeTab + '"]');

      this.$el.find('.etch-tabs__content--active').removeClass('etch-tabs__content--active');
      this.$el.find('.etch-tabs__anchor--active').removeClass('etch-tabs__anchor--active');
      $tabContent.addClass('etch-tabs__content--active');
      $tabAnchor.addClass('etch-tabs__anchor--active');
    },

    activateFromHash: function() {
      var tabHash = window.location.hash.replace('#', '');

      this.$el.find('.etch-tabs__anchor').removeClass('etch-tabs__anchor--active');

      if (tabHash.search('-tab') > -1) {
        this.$el.find('li.etch-tabs__anchor--active').removeClass('etch-tabs__anchor--active');
        this.$el.find('.etch-tabs__content--active').removeClass('etch-tabs__content--active');
        this.$el.find('[data-etch-tabs-anchor="' + tabHash + '"]').addClass('etch-tabs__anchor--active');
        this.$el.find('[data-etch-tabs="' + tabHash + '"]').addClass('etch-tabs__content--active');
      }

      if (this.$el.find('[data-etch-tabs="' + tabHash + '"].etch-tabs__content--active').length === 0) {
        this.setDefaultActive();
      }
    },

    setDefaultActive: function() {
      var $defaultTab;
      _.each(this.tabs, function(elm){
        if(elm.default) {
          $defaultTab = elm;
        }
      });

      if($defaultTab) {
        this.toggleVisible($defaultTab.name + '-tab');
      } else {
        this.$el.find('.etch-tabs__nav .etch-tabs__anchor').eq(0).addClass('etch-tabs__anchor--active');
        this.$el.find('.etch-tabs__content').eq(0).addClass('etch-tabs__content--active');
      }
    },

    destroy: function() {
      $(window).off('.etch-tabs');
      this.$el.off();
      this.$el.remove();
      this.$el = null;
      return this;
    }
  };

  window.Etch = _.assign(Etch, {tabs: tabs});

})();
