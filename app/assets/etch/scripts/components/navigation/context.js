(function(){
  "use strict";

  var Etch = window.Etch || {};

  var context = {
    $dropdown: null,
    $toggle: null,

    callback: function(el) {
      var self = this;
      this.$dropdown = $(el.dropdown);
      this.$toggle = $(el.toggle);
      this.$dropdown.one('click.etch.context-menu', '.etch-standard-dropdown__link', function(e) {
        e.preventDefault();
        var $target = $(e.currentTarget);
        self.$toggle.text($target.text());
        self.$toggle.attr('data-etch-context-value', $target.attr('href'));
        $.event.trigger({
          type: "etchContextMenuChange",
          context: self.$toggle,
          target: $target,
          time: new Date()
        });
      });
    }
  };

  $(document).on('etchContextMenuChange', function(data){
    data.context.append(' <i class="fa fa-sort-desc etch-context-menu__caret"></i>');
  });

  window.Etch = _.assign(Etch, {context: context});

})();
