(function(){
  "use strict";

  var Etch = window.Etch || {};

  var beacon = function() {
    this.$el = null;
    this.count = 0;

    this.init = function(el) {
      this.$el = $(el);
      this.count = parseInt(this.$el.find('.etch-beacon__count').text());
      this.wire();
      return this;
    };

    this.wire = function() {
      var showBlip = this.showBlip.bind(this);
      setTimeout(showBlip, 1500);

      this.$el.find('.etch-beacon').addClass('etch-beacon__count--show');
      this.$el.find('.etch-beacon--loading').addClass('etch-beacon--open');
    };

    this.update = function(count) {
      var showBlip = this.showBlip.bind(this);
      this.count = count;
      this.pulse(this.count);

      setTimeout(showBlip, 1000);
    };

    this.showBlip = function() {
      var showCount = this.showCount.bind(this);
      // make sure element is still available
      if (!this.$el.length) { return; }

      this.$el.find('.etch-beacon__count').text(this.count);
      this.loading();

      setTimeout(showCount, 150);
    };

    this.showCount = function() {
      this.$el.find('.etch-beacon__wrapper').addClass('etch-beacon--blip');
      this.$el.find('.etch-beacon__count').addClass('etch-beacon__count--show');
    };

    this.loading = function() {
      this.$el.find('.etch-beacon__wrapper').removeClass('etch-beacon--loading etch-beacon--open')
                                            .addClass('etch-beacon--show');
    };

    this.pulse = function(newCount) {
      this.$el.find('.etch-beacon__count').removeClass('etch-beacon__count--show')
                                          .text(newCount ? newCount : this.count);

      this.$el.find('.etch-beacon__wrapper').removeClass('etch-beacon--show etch-beacon--blip')
                                            .addClass('etch-beacon--loading');
    };

    this.show = function() {
      this.$el.show();
      this.$el.find('.etch-beacon__wrapper').removeClass('etch-beacon--loading etch-beacon--open')
                                            .addClass('etch-beacon--show etch-beacon--blip');
      this.$el.find('.etch-beacon').show();
      this.$el.find('.etch-beacon__count').addClass('etch-beacon__count--show');
    };

    this.hide = function() {
      this.$el.hide();
    };

    this.destroy = function() {
      this.$el.remove();
      this.$el = null;
    };
  };

  window.Etch = _.assign(Etch, {beacon: beacon});

})();
