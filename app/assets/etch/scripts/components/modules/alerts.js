(function(){
  "use strict";

  var Etch = window.Etch || {};

  var alerts = {
    $el: null,

    init: function(el) {
      _.bindAll(this);

      this.$el = el ? $(el) : $('.etch-alert');

      this.$el.on('click.etch-alert-close', '.etch-alert__close', this.hide);
      $(document).on('keydown.etch-alert-close', (function(_this) {
        return function(event) {
          var keyCode, ESCAPE_KEY;
          keyCode = event.keyCode || event.which;
          ESCAPE_KEY = 27;
          if (keyCode === ESCAPE_KEY && this.$el.hasClass('etch-alert--show')) {
            return _this.hide();
          }
        };
      })(this));

      return this;
    },

    show: function(options) {
      // Function to display stylized flash message
      // options - The Hash options used to refine the selection (default: {}):
      //           :message  - The message you want displayed (required)
      //           :title    - The title of the mand (optional)
      //           :type     - The type of flash message (success, error, info, warning : defaults to info)
      //           :duration - milliseconds to show the message or 'stay' (optional : defaults to 10s)
      if(!options.message) {
        console.log('A message is required for your Etch Alert');
        return;
      }

      var defaultTitle, duration, etchAlert, title, type, typeClass;

      type = options.type ? options.type : 'info';
      typeClass = "etch-alert--" + type;
      duration = options.duration ? options.duration : '10000';
      this.$el.removeClass('etch-alert--hidden');

      switch (type) {
        case 'info':
          defaultTitle = 'Notice';
          break;
        case 'success':
          defaultTitle = 'Success';
          break;
        case 'warning':
          defaultTitle = 'Warning';
          break;
        case 'error':
          defaultTitle = 'Error';
          break;
        default:
          defaultTitle = 'Notice';
          break;
      }

      title = options.title ? options.title : defaultTitle;

      this.$el.addClass(typeClass);
      this.$el.find('.etch-alert__title').text(title);
      this.$el.find('.etch-alert__message').text(options.message);

      this.$el.addClass("etch-alert--show");

      if (duration !== 'stay') {
        return setTimeout((function(_this) {
          return function() {
            return _this.hide();
          };
        })(this), duration);
      }
    },

    hide: function() {
      this.$el.removeClass("etch-alert--show");

      this.$el.one('transitionend', (function(_this) {
        return function() {
          _this.$el.toggleClass("etch-alert--hidden");
        };
      })(this));
    },

    toggle: function() {
      if(this.$el.hasClass('etch-alert--show')) {
        this.hide();
      } else {
        this.$el.removeClass('etch-alert--hidden');
      }
    },

    destroy: function() {
      this.$el.off('etch-alert-close');
      $(document).off('etch-alert-close');
      this.$el.remove();
      this.$el = null;
    }
  };

  window.Etch = _.assign(Etch, {alerts: alerts});

})();
