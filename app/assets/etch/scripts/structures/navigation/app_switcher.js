(function(){
  "use strict";

  var Etch = window.Etch || {};

  var appSwitcher = {
    $dropdown: null,
    $toggle: null,

    callback: function(el) {
      var self = this;
      this.$dropdown = $(el.dropdown);
      this.$toggle = $(el.toggle);
      $(document).on('etchContextMenuChange', function(e) {
        e.preventDefault();
        e.context.parents('.etch-app-switcher__list-item').find('.etch-btn').attr('href', e.message);
      });
    }
  };

  window.Etch = _.assign(Etch, {appSwitcher: appSwitcher});

})();
