(function(){
  "use strict";

  var Etch = window.Etch || {};

  var attribution = {
    $el: null,

    init: function() {
      this.$el = $('.etch-attribution');
      _.bindAll(this);
      this.$el.on('click', '.etch-attribution__option', this.onAttributionOptionClicked);
    },

    angularSupport: function ($select) {},

    onAttributionOptionClicked: function(event) {
      var $target = $(event.currentTarget),
          $scope = $target.parents('.etch-attribution'),
          idVal = $target.data('etch-attribution-value'),
          name = $target.find('.etch-attribution__value').text(),
          avatar = $target.find('.etch-attribution__avatar'),
          $select = $('.etch-attribution__select', $scope),
          $current = $('.etch-attribution__current', $scope);
                        
      $select.find('option[value="'+idVal+'"]').attr("selected","selected");            
      $current.find('.etch-attribution__value').text(name);
      this.angularSupport($select);

      if(avatar) {
        $current.find('.etch-attribution__avatar').html(avatar.html());
      }
    }
  };

  window.Etch = _.assign(Etch, {attribution: attribution});

})();
