(function(){
  "use strict";

  var Etch = window.Etch || {};

  var intel = {
    $el: null,

    init: function() {
      this.$el = $('.etch-modules-intel__wrapper');

      this.$el.off(".etch.intelModule");
      this.$el.on("click.etch.intelModule", ".etch-intel__type-filter", function() {

        var indicatorType = $(this).data('indicator-type');

        if(indicatorType == 'all') {
          $('.etch-intel__indicator-row').removeClass('hide');
        } else {
          $('.etch-intel__indicator-row').addClass('hide');
          $('.etch-intel__indicator-row--' + indicatorType).removeClass('hide');
        }

        $('.etch-intel__type-filter').removeClass('etch-active');
        $(this).addClass('etch-active');
      });

      this.$el.on("click.etch.intelModule", ".etch-intel__more-threat.alerts-link, .etch-intel__less-threat.alerts-link", function() {
        $(this).siblings(".etch-intel__toggle-overflow")
               .addBack()
               .toggle();
      });
    }
  };

  window.Etch = _.assign(Etch, {intel: intel});

})();
