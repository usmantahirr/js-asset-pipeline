(function(){
  "use strict";

  var Etch = window.Etch || {};

  var header = {
    search: {
      focus: function(data) {
        if(data) {
          $(data.dropdown).find('.etch-search--input').focus();
        }
      }
    }
  };

  window.Etch = _.assign(Etch, {header: header});

})();
