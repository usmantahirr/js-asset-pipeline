## Asset Pipeline
Test build system for Gulp, SASS and Angular Project

## Checklist
### Required Stuff
1. Bower dependency Injection
2. JS Concatination and Uglification
3. HTML Minification
4. copy fonts, images and templates
5. Fonts copier
6. SASS Compiler
7. Constant files generater

### Good to Have stuff
1. Images Compresser
2. Assets(images, fonts, SVG's) Version management