/**********************************
 * Plugins Loading
 **********************************/
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    compass = require('gulp-compass'),
    minifyHTML = require('gulp-minify-html'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    history = require('connect-history-api-fallback'),
    connect = require('gulp-connect'),
    usemin = require('gulp-usemin'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css'),
    clean = require('gulp-clean'),
    wiredep = require('wiredep').stream;

/**********************************
 * Source Specifications
 **********************************/

var jsSources = [
    'app/app.js',
    'app/shared/**/*.js',
    'app/components/**/*.js'
];
var etchSources = ['app/assets/etch/scripts/**/*.js'];
var templates = ['app/**/*.tpl.html'];
var index = ['app/index.html'];
var sassSources = ['app/app.scss'];
var images = ['app/assets/images/*.{jpg,jpeg,gif,png,svg}'];
var translations = ['app/i18n/**/*.json'];
var bower = 'bower_components';
var fontAwesome = 'font-awesome/fonts/**/*.{ttf,woff,eof,svg,eot,otf,woff2}';
/**
 * Build Folders
 */
var development = 'builds/development/';
var production = 'builds/production/';

/**********************************
 * Tasks
 **********************************/

/**
 * Task to Log stuff (Imported from Gulp-Utils)
 */
gulp.task('log', function () {
    gutil.log('Gulp Workflows Example');
});

/**
 * Clean folders
 */
gulp.task('clean', function () {
    gulp.src('builds')
        .pipe(clean())
        .on('error', gutil.log);
});

/**
 * Task for JavaScript Concatination.
 */
gulp.task('js', function () {
    gulp.src(jsSources)
        .pipe(concat('app.js'))
        .on('error', gutil.log)
        .pipe(gulp.dest(development + 'js'))
        .pipe(connect.reload());
});

/**
 * Task for JavaScript Uglification
 */
gulp.task('uglify', function() {
    console.log('uglify Task');
});

/**
 * Task for SASS Compilation using Compass
 */
gulp.task('compass', function () {
    gulp.src(sassSources)
        .pipe(compass({
            sass: 'app',
            image: 'app/assets/images/',
            style: 'expanded'
        }))
        .on('error', gutil.log)
        .pipe(gulp.dest( development + 'css'))
        .pipe(connect.reload());
});

/**
 * Task to copy index
 */
gulp.task('copyIndex', function() {
    gulp.src(index)
        .pipe(minifyHTML())
        .pipe(gulp.dest(development))
        .pipe(connect.reload());
});

/**
 * Task to Copy templates
 */
gulp.task('copyTemplates', function() {
    gulp.src(templates)
        .pipe(minifyHTML())
        .pipe(gulp.dest(development))
        .pipe(connect.reload());
});
/**
 * Copy Fonts
 */
gulp.task('copyFontAwesome', function() {
    gulp.src( bower + '/' + fontAwesome)
        .pipe(gulp.dest(development + 'fonts'));
});

/**
 * Copy Images
 */
gulp.task('copyImages', function() {
    gulp.src(images)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(development + 'images'))
        .pipe(connect.reload());
});

/**
 * Copy Translations
 */
gulp.task('copyTraslations', function() {
    gulp.src(translations)
        .pipe(gulp.dest(development + 'i81n'))
        .pipe(connect.reload());
})

/**
 * Copy Etch Scripts
 */
gulp.task('copyEtch', function() {
    gulp.src(etchSources)
        .pipe(gulp.dest(development + 'assets/etch/scripts'))
        .on('error', gutil.log);
});

/**
 * Task for Plugin file Concatination.
 */
gulp.task('plugins', function () {
    gulp.src('app/assets/plugins/**/*.js')
        .pipe(concat('plugins.js'))
        .on('error', gutil.log)
        .pipe(gulp.dest(development + 'js'))
        .pipe(connect.reload());
});

/**
 * Bower
 */
gulp.task('wiredep', function() {
    gulp.src(index)
        .pipe(wiredep())
        .on('error', gutil.log)
        .pipe(gulp.dest('app'));
});

/**
 * Creating Build Blocks, its dependent on Wiredep
 */
gulp.task('usemin', function () {
  return gulp.src(index)
      .pipe(usemin({
        // css: [minifyCSS(), 'concat'],
        // js: [uglify()]
      }))
      .on('error', gutil.log)
      .pipe(gulp.dest(development));
});

/**********************************
 * Runner Tasks
 **********************************/

/**
 * Task to manage Watch List
 */
gulp.task('watch', function() {
    gulp.watch('app/**/*.scss', ['compass']);
    gulp.watch(jsSources, ['js']);
    gulp.watch(index, ['processIndex']);
    gulp.watch(templates, ['copyTemplates']);
    gulp.watch(images, ['copyImages']);
    gulp.watch(etchSources, ['copyEtch']);
    gulp.watch(translations, ['copyTraslations']);
});

/**
 * Server & liveReload
 */
gulp.task('connect', function() {
    connect.server({
        directoryListing : true,
        fallback         : 'index.html',
        root             : development,
        port             : '9000',
        livereload       : true
    });
});

gulp.task('processIndex', ['wiredep', 'usemin', 'copyIndex']);

/**
 * Default Task (Initiate on issuing gulp)
 */
gulp.task('default', ['js', 'plugins', 'compass', 'processIndex', 'copyFontAwesome', 'copyImages', 'copyTraslations', 'copyEtch', 'connect']);
gulp.task('build-dev', ['js', 'plugins', 'compass', 'processIndex', 'copyFontAwesome', 'copyImages', 'copyTraslations', 'copyEtch', 'connect']);

